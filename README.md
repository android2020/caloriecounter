ECHIPA:
- Codreanu Andreea Alexandra - 405 - andreea.codreanu127@gmail.com
- Florea Andrada - 405 - andra.art97@yahoo.com
- Pauna Andreea Alexandra - 405 - alexandrapauna927@yahoo.com

CERINTE:
- operatiune cu camera(facut poze)
- Recycler View cu functie de cautare
- Navigation Drawer
- Share
- Firebase Notifications
- animatie cu ObjectAnimator
- UI pentru landscape mode
- Persistenta datelor (Room)
- Video Playback

DEMO:
https://drive.google.com/file/d/1aT6_xCqGYJglteXiVEZeXOiKO34unjul/view?fbclid=IwAR0U2dkWEAlPW5BJM12fyNZekMQGP611BT6ziZs-Z1LiT1GcD-HU3SqXWSY

DOCUMENTATIE CONFLUENCE:
https://andreeac.atlassian.net/wiki/spaces/ANDROID202/overview?fbclid=IwAR3Dyc4i9x_tC6PBiE2lf6phBGiy1l3q7FpYU7mX8dxqZXWcVErJdrWqsKQ